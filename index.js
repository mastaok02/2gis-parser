import { Builder, By, until } from 'selenium-webdriver'
import { xpath, css, scroll, error } from './const.js'
const { CITY: city, TARGET: target } = process.env
let count = 0
console.log(process.env)
;(async function example() {
  let isDone = false
  let results = []
  let subject = encodeURIComponent(target)
  let baseUrl = `https://2gis.kz/${city}/search/` + subject + '/'
  let driver = await new Builder().forBrowser('firefox').build()
  let pagesXpath2 = `.//a[contains(@href,'/${city}/search/${subject}/page')]`

  try {
    await driver.get(baseUrl)
  } catch (err) {
    console.log('err while opening website' + err)
  }

  try {
    await driver.manage().window().maximize()
    try {
      let cookieMenuClose = await driver.wait(until.elementLocated(By.css(css.cookie)), 10000)
      await cookieMenuClose.getRect()
      await cookieMenuClose.click()
    } catch (err) {
      console.log('err while closing cookie', err)
    }

    await driver.sleep(2000)
    await iterateOver(driver, results)
    await driver.executeScript(scroll.PageDown)
    let children = await driver.findElements(By.xpath(pagesXpath2))
    let mainActions = driver.actions({ async: true })

    while (!isDone) {
      try {
        let nextPage = await driver.wait(until.elementLocated(By.xpath(xpath.nextPage)), 5000)
        await driver.executeScript(scroll.To, nextPage)
        await nextPage.getRect()
        await nextPage.click()
        await driver.sleep(3000)
        await iterateOver(driver, results)
      } catch (err) {
        if (err.name === error.Timeout) {
          isDone = true
          break
        } else {
          console.log(err)
        }
      }
    }
    console.log('counter: ', count)

    const createCsvWriter = require('csv-writer').createObjectCsvWriter
    const csvWriter = createCsvWriter({
      path: 'out.csv',
      header: [
        { id: 'description', title: 'Описание' },
        { id: 'phone', title: 'Контакты' },
        { id: 'name', title: 'Название' },
        { id: 'address', title: 'Адрес' },
        { id: 'orderable', title: 'Бронь' },
        { id: 'link', title: 'Ссылка' },
      ],
    })

    await csvWriter.writeRecords(results)
    console.log('The CSV file was written successfully')
  } catch (err) {
    console.log('err att example() func', err)
  } finally {
    await driver.quit()
  }
})()

async function iterateOver(driver, results) {
  let blocks = await driver.findElements(By.xpath(xpath.allBlocks))
  for (let j = 0; j < blocks.length; j++) {
    try {
      let info = await getInfo(j + 1, driver)
      let json = JSON.stringify(info)
      count++
      println(count, json)
      let tempObj = {
        description: capitalize(info.value.description),
        phone: capitalize(info.value.phone.toString()),
        name: capitalize(info.value.name),
        address: capitalize(info.value.address),
        link: capitalize(info.value.link),
      }
      tempObj.orderable = info.value.orderable ? 'Да' : 'Нет'

      results.push(tempObj)
    } catch (err) {
      console.log('Error above: ', err)
      return
    }
  }
}

async function getInfo(i, driver) {
  return new Promise(async (res, rej) => {
    let nameBlock
    let orderBlock
    let orderExists = true

    const result = {
      success: true,
      message: null,
      value: {
        description: 'нет данных',
        phone: [],
        name: null,
        address: null,
        orderable: false,
        link: 'нет ссылки',
      },
    }
    let actions = driver.actions({ async: true })

    // Load page and get block of goal
    try {
      let mainBlock = await driver.wait(until.elementLocated(By.xpath(getMainBlockByXpath(i))), 10000)
      try {
        nameBlock = await driver.wait(until.elementLocated(By.xpath(getNameBlockByXpath(i, true))), 5000)
        result.value.name = await nameBlock.getAttribute('textContent')
      } catch (err) {
        if (err.name === error.Timeout) {
          nameBlock = await driver.wait(until.elementLocated(By.xpath(getNameBlockByXpath(i, false))), 5000)
          result.value.name = await nameBlock.getAttribute('textContent')
        } else {
          throw err
        }
      }

      try {
        orderBlock = await mainBlock.findElement(By.xpath(xpath.orderBlock))
      } catch (err) {
        if (err.name === error.NoElement) {
          result.value.link = 'нет ссылки'
          orderExists = false
        } else {
          throw err
        }
      }

      if (orderExists) {
        try {
          let link = await orderBlock.findElement(By.xpath(xpath.parentBlock))
          let linkText = await link.getAttribute('href')
          result.value.link = linkText
          result.value.orderable = true
        } catch (err) {
          result.value.link = 'нет ссылки'
          throw err
        }
      }

      // Open the block
      try {
        await driver.executeScript(scroll.To, nameBlock)
        await actions.move({ origin: nameBlock }).click().perform()
        await driver.sleep(1000)

        try {
          let descElement = await driver.wait(until.elementLocated(By.css('._cp8dm8')), 10000)
          try {
            result.value.description = await descElement.getText()
          } catch (err) {
            result.success = false
            result.message = err
            result.value = null
          }
        } catch (err) {
          result.value.description = 'Нет данных'
        }

        if (result.success) {
          try {
            let address = await driver.wait(until.elementLocated(By.xpath(xpath.address)), 10000)
            result.value.address = await address.getAttribute('textContent')
          } catch (err) {
            if (err.name === error.Timeout) {
              result.value.address = 'Нет данных'
            } else {
              result.success = false
              result.message = err
              result.value = null
            }
          }
        }

        // Click "show phone" then get phone
        if (result.success) {
          let phoneExists = true
          try {
            await driver.sleep(3500)
            let phoneBlock = await driver.wait(until.elementLocated(By.xpath(xpath.phoneBlock)), 15000)
            await driver.executeScript(scroll.To, phoneBlock)
            let position = await phoneBlock.getRect()
            await phoneBlock.click()
          } catch (err) {
            if (err.name === error.Timeout) {
              phoneExists = false
            } else {
              throw err
            }
          }

          if (phoneExists === true) {
            await driver.sleep(3500)
            try {
              await driver.wait(until.elementLocated(By.xpath('//bdo')), 10000)
              let phones = await driver.findElements(By.xpath('//bdo'))
              for (let j = 0; j < phones.length; j++) {
                result.value.phone.push(await phones[j].getAttribute('textContent'))
              }
            } catch (err) {
              result.success = false
              result.message = err
              result.value = null
            }
          }
        }
      } catch (err) {
        result.success = false
        result.message = err
        result.value = null
      }
    } catch (err) {
      console.log('Main error: ', err)
      result.success = false
      result.message = err
      result.value = null
    }

    result.success ? res(result) : rej(result)
    await driver.executeScript('window.history.go(-1)')
  })
}

function println(count, json) {
  console.log('\n----------------------------\n' + count + ':\n' + json + '\n----------------------------\n')
}

function getNameBlockByXpath(i, regular) {
  return regular ? xpath.allBlocks + `[${i}]/div/div/a/span` : xpath.allBlocks + `[${i}]/div/div/div[2]/span`
}

function getMainBlockByXpath(i) {
  return xpath.allBlocks + `[${i}]`
}

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

//*[@id="root"]/div/div/div[1]/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/div/div[3]/div[2]/div[2]
//*[@id="root"]/div/div/div[1]/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/div/div[4]/div[2]/div[2]
