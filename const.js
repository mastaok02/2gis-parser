export const xpath = {
  address: ".//a[contains(@href,'/geo/')]",
  phoneBlock: '//div[@class="_b0ke8"]//a[@class="_2lcm958"]',
  pages: '//div[@class="_12wz8vf"]//div//a',
  orderBlock: './/div[@class="_5kaapu" and (contains(text(), "Запис") or contains(text(), "запис"))]',
  parentBlock: './..',
  allBlocks: `/html/body/div[2]/div/div/div[1]/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/div/div[2]/div/div`,
  nextPage: `//*[@id="root"]/div/div/div[1]/div[1]/div[2]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div/div/div[3]/div[2]/div[2]`,
}

export const scroll = {
  PageDown: 'window.scrollTo(0, document.body.scrollHeight)',
  To: 'arguments[0].scrollIntoView();',
}

export const css = {
  cookie: '#root > div > div > div._hv3n7am > footer > div._euwdl0 > svg',
}

export const error = {
  Timeout: 'TimeoutError',
  NoElement: 'NoSuchElementError',
}

//'//div[@class="_b0ke8"]//a[@class="_2lcm958"]'
