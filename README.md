# 2GIS Parser

### 2GIS Parser - is useful to parse data from [2GIS](https://2gis.kz/) pointing city and target.

### As the result it creates Excel file "out.csv".

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Types of data to be parsed

- Name
- Description
- Address
- Phone
- Orderable
- Link to order

## Installation

Before using this bot, you must download the latest [geckodriver release](https://github.com/mozilla/geckodriver/releases/) and ensure it can be found on your system [PATH](http://en.wikipedia.org/wiki/PATH_%28variable%29).
2GIS parser requires [Node.js](https://nodejs.org/) v16+ to run.

Install the dependencies and devDependencies and start the server.

```sh
cd 2gis-parser
npm install
```

## Run

```sh
npm run start
```

## Configuration

To point CITY and TARGET change values in `.env` file.
